name := "ugs"

version := "2.4.11"

scalaVersion := "2.13.1"
enablePlugins(PackPlugin)

PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.26",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)

retrieveManaged := true
