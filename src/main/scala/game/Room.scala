package game

import java.io.{BufferedReader, InputStreamReader}
import java.net.{InetSocketAddress, URL}

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, Props}
import akka.io.{IO, Tcp}
import com.google.protobuf.ByteString
import main.{GetRoomState, Server}
import ru.enpire.ugs.protocol.messages.RoomCommand.{Leave, Ping, SyncRequest}
import ru.enpire.ugs.protocol.messages.{GameRoomMessage, GameRoomVariables, UpdateGameRoomVariableRequest, UserEntered}
import transport.{EchoTcp, EchoTcpPortBound, EchoUdp, EchoUdpPortBound, RoomPeer}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object Room {
  def props(bundleId: String, version: String, options: String): Props = Props(new Room(bundleId, version, options))
}

class Room(bundleId: String, version: String, options: String) extends Actor
  with ActorLogging
{

  import akka.io.Tcp._
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(Server.host, 0))

  var playersCount = 0
  var address = ""
  var port = 0
  var echoPortUdp = 0
  var echoPortTcp = 0
  val defaultIP: String = Server.host
  var stage = 0
  var timeout = 30000

  val startTime: Long = System.currentTimeMillis()
  val variables: mutable.Map[String, ByteString] = mutable.Map[String, ByteString]()
  val variableOwners: mutable.Map[String, VariableOwner] = mutable.Map[String, VariableOwner]()
  val playersActivity: mutable.Map[Int, Long] = mutable.Map[Int, Long]()
  val shutdownTimeout: Cancellable = system.scheduler.schedule(timeout millis, timeout millis, self, ShootdownCheck)

  // Start echo services
  context.actorOf(Props[EchoTcp])
  context.actorOf(Props[EchoUdp])

  println("Room created")


  def ready: Boolean = {
    port > 0 && echoPortTcp > 0 && echoPortUdp > 0
  }

  def receive = {
    case EchoTcpPortBound(p) =>
      echoPortTcp = p

    case EchoUdpPortBound(p) =>
      echoPortUdp = p

    case b @ Tcp.Bound(_) =>
      address = getExternalIP
      port = b.localAddress.getPort
      println("Bound TCP: " + port)

    case CommandFailed(_: Bind) => context stop self

    case GetRoomState =>
      if (ready && playersCount < 128)
        sender() ! state
      else
        sender() ! RoomNotReady

    case r: RoomRequest =>
      println("Room received: " + r)
      if (r.bundleId == bundleId && r.version == version) {
        var matched = true
        r.conditionStageMax.foreach(stageMax => {
          if (stage > stageMax) matched = false
        })
        r.conditionStageMin.foreach(stageMin => {
          if (stage < stageMin) matched = false
        })
        r.conditionActivePlayersMin.foreach(playersCount => {
          if (activePlayersCount < playersCount) matched = false
        })
        r.conditionActivePlayersMax.foreach(playersCount => {
          if (activePlayersCount > playersCount) matched = false
        })

        if (matched) {
          println("Matched: " + r )
          r.requesting ! state
        }
      }

    case c @ Connected(remote, local) =>
      log.debug(s"Connected ${remote.getAddress}:${remote.getPort}")
      println(s"Connected ${remote.getAddress}:${remote.getPort}")
      val handler = context.actorOf(RoomPeer.props(sender(), remote, playersCount + 1))
      handler ! GameRoomMessage(playersCount, GameRoomMessage.MessageContent.UserEntered(UserEntered(playersCount)))
      playersCount = playersCount + 1
      sender() ! Register(handler)

    case message: GameRoomMessage =>
      playersActivity.update(message.userId, System.currentTimeMillis())
      message.messageContent match {
        case m: GameRoomMessage.MessageContent.Command =>
          m.command.get match {
            case Ping =>
            case SyncRequest =>
              sender() ! GameRoomMessage(message.userId, GameRoomMessage.MessageContent.RoomVariables(GameRoomVariables(variables.toMap)))
              context.children.foreach(_ ! message)
            case Leave =>
              context.children.foreach(_ ! message)
            case _ =>
          }

        case m: GameRoomMessage.MessageContent.BroadcastMessage =>
          context.children.foreach(_ ! message)

        case m: GameRoomMessage.MessageContent.DirectMessage =>
          context.children.foreach(_ ! message)

        case m: GameRoomMessage.MessageContent.UpdateVariablesRequest =>
          val validRequests = m.value.requests.filter(checkVariableOwnership(message.userId, _))
          validRequests.foreach(r => {
            variables.update(r.key, r.value)
            variableOwners.update(r.key, VariableOwner(
              message.userId,
              System.currentTimeMillis() + (1000L * r.lockTimeout).toLong
            ))
          })
          val updatedVariables = variables.filterKeys(validRequests.map(r => r.key).contains).toMap
          val msg = GameRoomMessage(0, GameRoomMessage.MessageContent.RoomVariables(GameRoomVariables(updatedVariables)))
          context.children.foreach(_ ! msg)

        case GameRoomMessage.MessageContent.SetStageRequest(r) =>
          println(s"SetStageRequest: $r")
          if (r.stage > stage)
            stage = r.stage

        case _ =>
      }

    case ShootdownCheck =>
      val now = System.currentTimeMillis()
      if (playersActivity.count(p => p._2 > (now - timeout)) < 1) {
        println("Shooting down")
        shutdownTimeout.cancel()
        context.stop(self)
      }

    case _ =>
  }

  def activePlayersCount: Int = {
    val now = System.currentTimeMillis()
    playersActivity.count(p => p._2 > (now - 5000))
  }

  def state: RoomState = {
    RoomState(
      bundleId,
      version,
      address,
      port,
      echoPortUdp,
      echoPortTcp,
      stage,
      activePlayersCount,
      options,
      System.currentTimeMillis() - startTime
    )
  }

  def checkVariableOwnership(userId: Int, updateRequest: UpdateGameRoomVariableRequest): Boolean = {
    !variableOwners.exists(entry => entry._1 == updateRequest.key && entry._2.playerId != userId && entry._2.unlockTime > System.currentTimeMillis())
  }

  private def getExternalIP = {
    var address = defaultIP
    if (Server.requestExternalIP) {
      try {
        val whatismyip = new URL("http://checkip.amazonaws.com")
        val in = new BufferedReader(new InputStreamReader(
          whatismyip.openStream()))

        address = in.readLine()
      }
      catch {
        case _: Throwable =>
      }
    }
    address
  }

}

case class VariableOwner(playerId: Int, unlockTime: Long)

case class ShootdownCheck()
case class RoomState(bundleId: String, version: String, address: String, port: Int, echoPortUdp: Int, echoPortTcp: Int, stage: Int, activePlayers: Int, options: String, timeFromStart: Float)
case class RoomNotReady()
case class RoomRequest(
                        bundleId: String,
                        version: String,
                        options: String,
                        conditionStageMin: Option[Int],
                        conditionStageMax: Option[Int],
                        conditionActivePlayersMin: Option[Int],
                        conditionActivePlayersMax: Option[Int],
                        requesting: ActorRef
                      )
