package transport

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.ByteString
import game.{RoomNotReady, RoomRequest, RoomState}
import main.{CreateRoomRequest, GetRoomState, RoomCreated, Server}
import ru.enpire.ugs.protocol.messages.Packet.PacketContent
import ru.enpire.ugs.protocol.messages.{GameRoomList, GameRoomProps, Packet}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

object GamePeer {

  def props(connection: ActorRef, remote: InetSocketAddress): Props = Props(new GamePeer(connection, remote))
}

class GamePeer(connection: ActorRef, remote: InetSocketAddress) extends Actor
  with TcpPeer
  with ActorLogging
{
  import akka.io.Tcp._
  import context.system

  context watch connection
  var peer = connection
  var activeRoomRequest: Option[RoomRequest] = None

  def handleData(data: ByteString): Unit = {
    val packet = Packet.parseFrom(data.toArray)
    packet.packetContent match {
      case Packet.PacketContent.RequestGameRoom(r) =>
        r.props.foreach(requestedProps => {
          r.conditions.foreach(conditions => {
            activeRoomRequest = Some(RoomRequest(
              requestedProps.bundleId,
              requestedProps.version,
              requestedProps.options,
              conditions.conditionStageMin.stageMin,
              conditions.conditionStageMax.stageMax,
              conditions.conditionActivePlayersMin.playersMin,
              conditions.conditionActivePlayersMax.playersMax,
              self
            ))
            activeRoomRequest.foreach(r => Server.roomsManager ! r)
            system.scheduler.scheduleOnce(100 milliseconds, self, RoomNotFound)
          })
        })
      case _ =>
        Server.roomsManager ! packet
    }
  }

  def receive = tcpHandler orElse handler

  def handler: PartialFunction[Any, Unit] = {

    case packet: Packet =>
      val data = packet.toByteString
      self ! data

    case roomState: RoomState =>
      activeRoomRequest = None
      val roomProps = GameRoomProps(
        roomState.bundleId,
        roomState.version,
        roomState.address,
        roomState.port,
        roomState.stage,
        roomState.activePlayers,
        roomState.options,
        roomState.timeFromStart,
        roomState.echoPortUdp,
        roomState.echoPortTcp
      )
      self ! Packet(PacketContent.GameRoomList(GameRoomList(Seq(roomProps))))

    case RoomNotFound =>
      println(RoomNotFound)
      activeRoomRequest.foreach(r => {
        Server.roomsManager ! CreateRoomRequest(
          r.bundleId,
          r.version,
          r.options
        )
      })

    case RoomCreated(ref) =>
      ref ! GetRoomState

    case RoomNotReady =>
      context.system.scheduler.scheduleOnce(100 milliseconds, sender(), GetRoomState)

    case PeerClosed     =>
      println("Peer Closed")
      context stop self
  }
}

case class RoomNotFound()
