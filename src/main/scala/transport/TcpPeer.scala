package transport

import java.nio.ByteBuffer

import akka.actor.{Actor, ActorRef}
import akka.io.Tcp.{Received, Write}
import akka.util.ByteString

trait TcpPeer
  extends Actor
{

  var rememberedData: ByteString = _
  var peer: ActorRef

  def receiveData(data: ByteString): Unit = {
    if (rememberedData != null) {
      val rd = rememberedData
      rememberedData = null
      receiveData(rd.concat(data))
      return
    }
    if (data.length < 2) {
      rememberedData = data
      return
    }
    val size = 256 * (data(0) & 0xFF) + (data(1) & 0xFF)
    val tail = data.drop(2)
    if (tail.length < size) {
      rememberedData = data
      return
    }
    handleData(tail.take(size))
    if (tail.length > size) {
      receiveData(tail.drop(size))
    }
  }

  def handleData(data: ByteString)

  def tcpHandler: PartialFunction[Any, Unit] = {
    case Received(data) =>
      receiveData(data)

    case data: com.google.protobuf.ByteString =>
      self ! ByteString(data.toByteArray)

    case data: ByteString =>
      val b = ByteBuffer.allocate(2)
      b.putChar(data.length.toChar)
      peer ! Write(ByteString(b.array()).concat(data))
  }
}
