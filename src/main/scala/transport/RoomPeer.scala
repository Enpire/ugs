package transport

import java.net.InetSocketAddress
import java.nio.ByteBuffer

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.ByteString
import ru.enpire.ugs.protocol.messages.{GameRoomMessage, RoomCommand}
import ru.enpire.ugs.protocol.messages.GameRoomMessage.MessageContent
import ru.enpire.ugs.protocol.messages.RoomCommand.{Pause, Ping, Resume, SyncRequest, Unrecognized}

object RoomPeer {

  def props(connection: ActorRef, remote: InetSocketAddress, playerId: Int): Props = Props(new RoomPeer(connection, remote, playerId))
}

class RoomPeer(connection: ActorRef, remote: InetSocketAddress, playerId: Int)
  extends Actor with ActorLogging with TcpPeer {

  import akka.io.Tcp._
  context watch connection
  var peer = connection
  var paused = false

  def handleData(data: ByteString) = {
    val roomMessage = GameRoomMessage.parseFrom(data.toArray)
    if (roomMessage.messageContent.isCommand) {
      roomMessage.messageContent.command.get match {
        case Pause =>
          paused = true
        case Resume =>
          paused = false
        case _ =>
          context.parent ! roomMessage
      }
    }
    else context.parent ! roomMessage
  }

  def receive = tcpHandler orElse handler

  def handler: PartialFunction[Any, Unit] = {

    case m: GameRoomMessage =>
      if (m.messageContent.isDirectMessage) {
        if (m.messageContent.directMessage.get.userId == playerId) sendMessage(m)
      }
      else sendMessage(m)

    case PeerClosed     =>
      context.parent ! GameRoomMessage(
        playerId,
        GameRoomMessage.MessageContent.Command(RoomCommand.Leave)
      )
      println("Peer Closed")
      context stop self
  }

  def sendMessage(gameRoomMessage: GameRoomMessage): Unit = {
    if (paused) return
    val data = gameRoomMessage.toByteString
    self ! data
  }

}

