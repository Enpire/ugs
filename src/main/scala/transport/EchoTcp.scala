package transport

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import main.Server



class EchoTcp extends Actor
  with ActorLogging
{

  import akka.io.Tcp._
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(Server.host, 0))

  def receive = {
    case b @ Tcp.Bound(_) =>
      val port = b.localAddress.getPort
      println("Bound echo TCP: " + port)
      context.parent ! EchoTcpPortBound(port)

    case CommandFailed(_: Bind) => context stop self

    case c @ Connected(remote, local) =>
      println(s"Connected echo tcp ${remote.getAddress}:${remote.getPort}")
      val handler = context.actorOf(EchoTcpPeer.props(sender()))
      sender() ! Register(handler)

    case data: ByteString =>
      context.children.foreach(_ ! data)

    case _ =>
  }

}

case class EchoTcpPortBound(port: Int)
