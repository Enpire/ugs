package transport

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging}
import akka.io.{IO, Tcp}
import main.{Config, Server}

class TcpGate extends Actor
  with ActorLogging
{

  import akka.io.Tcp._
  import context.system


  IO(Tcp) ! Bind(self, new InetSocketAddress(Server.host, Config.tcpGatePort))


  def receive = {

    case b @ Bound(_) =>
      log.debug("Bound port")
      println("Bound port")
      context.parent ! b

    case CommandFailed(_: Bind) => context stop self

    case c @ Connected(remote, local) =>
      log.debug(s"Connected ${remote.getAddress}:${remote.getPort}")
      println(s"Connected ${remote.getAddress}:${remote.getPort}")
      val handler = context.actorOf(GamePeer.props(sender(), remote))
      sender() ! Register(handler)
  }

}
