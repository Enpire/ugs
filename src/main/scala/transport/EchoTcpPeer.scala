package transport

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.ByteString

object EchoTcpPeer {
  def props(connection: ActorRef): Props = Props(new EchoTcpPeer(connection))
}

class EchoTcpPeer(connection: ActorRef) extends Actor
  with ActorLogging
  with TcpPeer
{
  override var peer: ActorRef = connection

  override def receive: Receive = tcpHandler

  override def handleData(data: ByteString): Unit = {
    context.parent ! data
  }
}
