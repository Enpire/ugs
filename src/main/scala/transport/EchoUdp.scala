package transport

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.io.Udp.{Bind, CommandFailed}
import akka.io.{IO, Udp}
import main.Server

import scala.collection.mutable

class EchoUdp extends Actor
  with ActorLogging
{

  import context.system

  IO(Udp) ! Bind(self, new InetSocketAddress(Server.host, 0))

  var port = 0
  var udpSocket: ActorRef = _
  var udpRemotes: mutable.ListBuffer[InetSocketAddress] = new mutable.ListBuffer[InetSocketAddress]

  def receive = {
    case b @ Udp.Bound(_) =>
      udpSocket = sender()
      val port = b.localAddress.getPort
      println("Bound UDP: " + port)
      context.parent ! EchoUdpPortBound(port)

    case Udp.Received(data, remote) =>
      if (!udpRemotes.contains(remote)) {
        udpRemotes += remote
      }
      udpRemotes.foreach(remote => {
        udpSocket ! Udp.Send(data, remote)
      })

    case CommandFailed(_: Bind) => context stop self

    case _ =>
  }


}

case class EchoUdpPortBound(port: Int)
