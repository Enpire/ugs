package main

object Config {

  lazy val tcpGatePort: Int = {
    scala.util.Properties.envOrNone("PORT").flatMap(_.toIntOption).getOrElse(11000)
  }

  lazy val host: String = {
    scala.util.Properties.envOrElse("HOST", "sb.enpire.ru")
  }

  lazy val useExternalIp: Boolean = {
    scala.util.Properties.envOrElse("USE_EXTERNAL_IP", "true") == "true"
  }

}
