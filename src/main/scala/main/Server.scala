package main

import akka.actor.{ActorSystem, Props}
import transport.TcpGate

object Server extends App {

  lazy val host = Config.host
  val requestExternalIP = Config.useExternalIp
//  val requestExternalIP = false
//  val host = "5.189.163.129"
//  val host = "127.0.0.1"
//  val host = "192.168.1.110"

  val system = ActorSystem.create()


  val tcpGate = system.actorOf(Props[TcpGate])
  lazy val roomsManager = system.actorOf(Props[RoomsManager])

}
