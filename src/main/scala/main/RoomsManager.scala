package main

import akka.actor.{Actor, ActorLogging, ActorRef}
import game.{Room, RoomRequest}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class RoomsManager extends Actor
  with ActorLogging
{


  def futureToFutureOption[T](f: Future[T]): Future[Option[T]] =
    f.map(Some(_)).recover {
      case e => None
    }

  def receive = {
    case r: RoomRequest =>
      println("Room manager received: " + r)
      println("Children count: " + context.children.size)
      context.children.foreach(child => child ! r)

    case CreateRoomRequest(bundleId, version, options) =>
      sender() ! RoomCreated(context.actorOf(Room.props(
        bundleId,
        version,
        options
      )))

  }

}

case class GetRoomState()
case class RoomCreated(ref: ActorRef)
case class CreateRoomRequest(bundleId: String, version: String, options: String)
case class NoMatchingRooms(roomRequest: RoomRequest)
